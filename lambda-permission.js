const AWS = require("aws-sdk");

const awsInfo = {
    aws_id: "AKIAIKVFDPGR2NA2ZWDA",
    aws_key: "dYVPU0zHsD2ckWwroUMgGuttCKcjGV4xvAqI1Hvq",
    region: 'ap-northeast-1'
};

const lambda = new AWS.Lambda(awsInfo);

var apigateway = new AWS.APIGateway(awsInfo);

getApiResources([], null);

function getApiResources(items, position) {
    var params = {
        //restApiId: '8jt14da01e', /* required */
        restApiId: 'wmfssycid0', // legacy
        embed: [
        ],
        limit: 500,
        position: (position||null)
    };
    apigateway.getResources(params, function(err, data) {
        if (err) {
            console.log(err, err.stack);
        } // an error occurred
        else {
            getMethods(
                data.items.filter((v)=>{
                    return (typeof(v.resourceMethods)!='undefined');
                }).map((v)=>{
                    return {
                        id:v.id,
                        path: v.path,
                        resourceMethods:Object.keys(v.resourceMethods).filter((k)=>{
                            return (k!=='OPTIONS');
                        })
                    };
                })
            );

        }
    });
}

function getMethods(items) {
    let uris = {};
    let cur = 0;
    let total = items.length;
    let paths = {};
    items.forEach((item)=> {
        item.resourceMethods.forEach( (method) => {
            apigateway.getMethod(
                {
                    //restApiId: '8jt14da01e',
                    restApiId: 'wmfssycid0',
                    resourceId: item.id,
                    httpMethod: method
                },
                function (err, data) {
                    if (err) {
                        console.log(err);
                    } else {
                        //console.log(data.methodIntegration);
                        if(data.methodIntegration && data.methodIntegration.uri.indexOf('stageVariables')>0) {
                            uris[data.methodIntegration.uri] = data.methodIntegration.uri;

                            addPermission(item.path, data.methodIntegration.uri);
                        }
                        cur++;
                        if(cur==total) {
                            let arrUris = Object.keys(uris).filter((v)=>{
                                return v.indexOf('arn:aws:apigateway:ap-northeast-1:lambda')>=0;
                            })
                            //console.log(arrUris);
                            //createAlias(arrUris);
                        }
                    }
                }
            );
        });
    });
}

function addPermission(path, uri) {
    var params = {
        Action: "lambda:InvokeFunction",
        FunctionName: uri.match(/function\:(.+)\:\$/)[1],
        Principal: "apigateway.amazonaws.com",
        SourceAccount: "768556645518",
        SourceArn: "arn:aws:execute-api:ap-northeast-1:768556645518:wmfssycid0/*/*"+path.replace(/\{[^\}]+\}/, '*'),
        StatementId: "addpermission-zigbang-com-"+Date.now()
    };
    //console.log(params);
    lambda.addPermission(params, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else     console.log(data);           // successful response
        /*
         data = {
         Statement: "ID-1"
         }
         */
    });
}

function createAlias(arrUris) {
    arrUris.forEach((uri)=>{
        var params = {
            FunctionName: uri.match(/function\:(.+)\:\$/)[1], /* required */
            FunctionVersion: '$LATEST', /* required */
            Name: 'STAGE', /* required */
            Description: 'Stage for test'
        };
        console.log(params);

        lambda.createAlias(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else     console.log(data);           // successful response
        });
    });
}