const AWS = require("aws-sdk");
// 기본적으로는 로컬에 있는 정보 사용
const awsInfo = {
    aws_id: "AKIAIKVFDPGR2NA2ZWDA",
    aws_key: "dYVPU0zHsD2ckWwroUMgGuttCKcjGV4xvAqI1Hvq",
    region: 'ap-northeast-1'
}
const cloudwatchlogs = new AWS.CloudWatchLogs(awsInfo);
const lambda = new AWS.Lambda(awsInfo);

const excludeList = [
//    '/aws/lambda/zigbang-sale-apart-test',
//   '/aws/lambda/zigbang_pay'
];
const retentionMaxLimit = 7; // 허용할 날짜

const getList = (nextToken) =>
{
    var params = {
        limit: 50,
        logGroupNamePrefix: '/aws/lambda',
        nextToken: (nextToken || null)
    };
    cloudwatchlogs.describeLogGroups(
        params
        , function (err, data) {
            if(err) {
                // too many error 발생하면 처리하려했으나 에러가 안나서...
                console.log(err);
                return;
            }

            // process each function
            data.logGroups.forEach((lInfo)=>{
                // 100메가 이상 출력
                if(lInfo.storedBytes/1000000>100) {
                    //console.log(lInfo.logGroupName + " : " + Math.round(lInfo.storedBytes/1000000).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')+ " Mbytes");
                }

                if( (!lInfo.retentionInDays || lInfo.retentionInDays === 0 || lInfo.retentionInDays > retentionMaxLimit) &&
                    (excludeList.indexOf(lInfo.logGroupName) < 0)
                )
                {
                    console.log(lInfo.logGroupName + " : " + lInfo.retentionInDays);
                    // 일단 실행 부분 주석
                    setRetentionDay(lInfo.logGroupName, 1);
                }
            });

            // get next list
            if(data.nextToken) {
                //console.log("NEXT : "+data.nextToken);
                getList(data.nextToken);
            }
    });
}
getList();

const setRetentionDay = (logGroupName, day) => {
    let params = {
        logGroupName: logGroupName,
        retentionInDays: day
    };
    cloudwatchlogs.putRetentionPolicy(params, function(err, result){
    	console.log(err);
	console.log(result);
    });
}



